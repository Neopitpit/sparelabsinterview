

## Quick summary ##

* * *
## Version ##
Interview

* * *
## Setup ##
 1. Checkout code (git clone)
 2. Go into the project folder
 3. Execute command `npm install` (that will install all module dependencies)
 4. Run : `react-native run-ios` (Android version is not implemented, reason, did not finish to install mapbox)

 NOTE: Maybe it needs to change the team in the signing section into XCode.



## Features implemented ##

This a react NATIVE version (iOS only, the Android version is just the installation of mapbox but I did not test it)

For simplicity, there is only one view who contains the map, center on Vancouver and display ALL Translink buses. The buses position are refreshing every 2 seconds.


TRY: I tried to put a callout when user click on a dot (bus) but the callout is not showing. Did not find the issue with map-box

## Notes ##

* react-redux: I used redux in order to save the data return by Translink. The main view (map) refresh itself when the store contains new buses position.

* Global Parameter: Key and static value are defined in src/global/globalParameter.js. As an improvement, it can be possible to define parameters per environment with react-native-config for example.
8 Global color (theming): Started a file in order to define the theme of the app (it contains for now only 2 colors but it can be possible to add font, size of the button, tabbar etc, ...)


## Possible improvements ##
* A lot of improvement can be done on the map (Callout in order to display info about the bus), provide more details about the buses, ...
* Change the size of the dots (buses) depending the scale of the map
* Use react-navigation in order to implement another view (detail of a bus, ...)
* Use react-native-config in order to set parameters per environment (DEV, STAGING, PROD)
* Deployment with Microsoft AppCenter or Bitrise or Fastlane


## Answer of questions ##

Answer question 1:
The new technology I learn is react-map box, but globally it is similar to any other component using maps (like AirBnb map).
However I tried to implement a callout with Mapbox but apparently that does not work (I followed the documentation, maybe something I missed)

Answer question 2:
Time-consuming is the installation and the set of redux.
In terms of challenge, I took the solution to pull the data from Translink every 2 seconds. Maybe another solution is to pull when there is a change and pull data only for buses visible on the screen.

But in order to define the best solution it needs to define the global needs :-)
