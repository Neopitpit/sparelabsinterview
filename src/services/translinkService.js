/**
 * TransLinkService.js
 * @flow
 */

import API from 'sparelabsInterview/src/helpers/api';
import * as GlobalParameters from 'sparelabsInterview/src/global/globalParameters';

class TransLinkService {

  static getAllBuses() {
    const url = `${GlobalParameters.PROTOCOL}${GlobalParameters.API}${GlobalParameters.translinkEndPoints.buses}?apikey=${GlobalParameters.API_KEY}`;
    return API.get(url).catch((e) => {
      throw  e
    });
  }

}

export default TransLinkService;
