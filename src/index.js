/**
 * Officepools mobile app
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  Platform,
} from 'react-native';
import { AsyncStorage } from 'react-native';
import { Provider } from 'react-redux';
import Store from './store';

import Main from 'sparelabsInterview/src/scenes/main';

class MobileApp extends Component {
  state = { store: null };

  constructor(props) {
    super(props);
  }

  // Load the store
  componentWillMount() {
    this.setState({ store: Store });
  }

  render() {

    // If the store is loaded then display the main screen
    if (this.state.store) {
      return (
        <Provider store={this.state.store}>
          <Main />
        </Provider>
      );
    }

    return null;
  }
}

AppRegistry.registerComponent('sparelabsInterview', () => MobileApp);
