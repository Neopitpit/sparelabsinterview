class TaskManager {

  constructor() {
    this.tasks = {}
  }

  startTask = (name, callback, interval, replace=true) => {
    let task = this.tasks[name]

    // Let's see if the task already exists. If it exists, let's stop it.
    if(task && !replace) {
      return
    } else if(task) {
      this.stopTask(task)
    }

    // Let's start the new task
    this.tasks[name] = setInterval(callback, interval)
  }

  stopTask = (name) => {
    // Only do something if this attribute exists
    if(this.tasks[name]) {
      clearInterval(this.tasks[name])
      delete this.tasks[name]
    }
  }
}

// We're exporting a Singleton
export default new TaskManager()
