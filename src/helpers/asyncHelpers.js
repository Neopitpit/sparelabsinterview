/**
 * asyncHelpers.js
 * @flow
 */

export const makeCancellable = (promise) => {
  let hasCancelled_ = false;

  const wrappedPromise = new Promise((resolve, reject) => {
    promise
      .then(
        (val) => { hasCancelled_ ? reject({isCancelled: true}) : resolve(val); },
        (error) => { hasCancelled_ ? reject({isCancelled: true}) : resolve(error); }
      )
      .catch((error) => {
        if(hasCancelled_) {
          reject({isCancelled: true});
        } else {
          throw error;
        }
      });
  });

  return {
    promise: wrappedPromise,
    cancel() {
      hasCancelled_ = true;
    }
  };
}
