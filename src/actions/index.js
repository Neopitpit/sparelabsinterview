/**
 * index.js
 * @flow
 */

import * as TranslinkActions from './translink';

export const ActionCreators = Object.assign({},
  TranslinkActions,
);
