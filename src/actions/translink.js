/**
 * user.js
 * @flow
 */

import * as ActionTypes from './actionTypes';
import TransLinkService from 'sparelabsInterview/src/services/translinkService';

//////////////////////////////////////////////////////////////////////////////
// REGISTRATION //////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
export function setAllBusesAction({ buses }) {
  return {
    type: ActionTypes.TRANSLINK_BUSES_SUCCEED,
    buses
  };
}

export function setAllBusesPending() {
  return {
    type: ActionTypes.TRANSLINK_BUSES_PENDING,
  };
}

export function setAllBusesRejected({ error }) {
  return {
    type: ActionTypes.TRANSLINK_BUSES_REJECTED,
    error
  };
}

export function getAllBuses() {
  return (dispatch, getState) => {
    dispatch(setAllBusesPending());
    return TransLinkService.getAllBuses()
      .then((resp) => {
        dispatch(setAllBusesAction({ buses: resp }));
      }).catch((ex) => {
        dispatch(setAllBusesRejected({error: ex}));
        throw ex;
      })
  }
}
