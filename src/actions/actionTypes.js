/**
 * actionTypes.js
 * @flow
 */

export const NONE = 'NONE';

export const RESET_STORE = 'RESET_STORE';

// TRANSLINK
export const TRANSLINK_BUSES_PENDING = 'translink/BUSES_PENDING';
export const TRANSLINK_BUSES_SUCCEED = 'translink/BUSES_SUCCEED';
export const TRANSLINK_BUSES_REJECTED = 'translink/BUSES_REJECTED';
