import { StyleSheet } from 'react-native';

import GlobalColors from 'sparelabsInterview/src/global/globalColors';

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  annotationContainer: {
    width: 6,
    height: 6,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: GlobalColors.brandPrimary,
    borderRadius: 3,
  },
});
