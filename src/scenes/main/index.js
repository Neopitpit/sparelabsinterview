/**
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Alert,
} from 'react-native';
import { ActionCreators } from 'sparelabsInterview/src/actions';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Mapbox from '@mapbox/react-native-mapbox-gl';

// GLOBAL
import * as GlobalParameters from 'sparelabsInterview/src/global/globalParameters';
Mapbox.setAccessToken(GlobalParameters.MAPBOX_KEY);

// SERVICES
import  TransLinkService from 'sparelabsInterview/src/services/translinkService';

// HELPERS
import TaskManager from 'sparelabsInterview/src/helpers/taskManager';

// STYLES
import GlobalColors from 'sparelabsInterview/src/global/globalColors';
import styles from './styles';

export class Main extends Component<{}> {

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    TaskManager.startTask(GlobalParameters.PULL_ALL_BUSES_NAME, this.getBuses.bind(this), GlobalParameters.PULL_ALL_BUSES_RATE, false);
  }

  getBuses() {
    this.props.getAllBuses()
  }

  renderAnnotations () {

    var busesPosition = [];

    if(this.props.buses == null) {
      return
    }

    this.props.buses.forEach(function(item) {
      if( item.Latitude != null && item.Longitude != null) {
        let busesPositionKey = item.VehicleNo + item.Pattern;
        busesPosition.push(
          <Mapbox.PointAnnotation
            key={busesPositionKey}
            id={busesPositionKey}
            coordinate={[item.Longitude, item.Latitude]}>
              <View style={styles.annotationContainer}>
              </View>
            <Mapbox.Callout title={'Bus ' + item.VehicleNo} />
          </Mapbox.PointAnnotation>
        )
      }
    })

    return busesPosition;

  }

  render() {
    return (
      <View style={styles.container}>
        <Mapbox.MapView
          styleURL={Mapbox.StyleURL.Street}
          zoomLevel={10}
          centerCoordinate={[-123.1207, 49.2827 ]}
          showUserLocation={true}
          style={styles.container}>
            {this.renderAnnotations()}
        </Mapbox.MapView>
      </View>
    );
  }
}

// Meta
function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

function mapStateToProps(state) {
  return {
    buses: state.translink.buses,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Main, 'Main');
