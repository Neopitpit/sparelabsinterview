/**
 * userReducer.js
 * @flow
 */

import createReducer from 'sparelabsInterview/src/helpers/createReducers';
import * as actionTypes from 'sparelabsInterview/src/actions/actionTypes';

export type State = {
  error: ?string,
  buses: ?buses
}

export const initialState = {
  buses: [],
};

export const translink = createReducer({}, {
  [actionTypes.TRANSLINK_BUSES_SUCCEED](state: State, action) {
    return {
      ...state,
      buses: action.buses,
      error: null,
    };
  },
  [actionTypes.TRANSLINK_BUSES_PENDING](state: State, action) {
    return {
      ...state,
    };
  },
  [actionTypes.TRANSLINK_BUSES_REJECTED](state: State, action) {
    return {
      ...state,
      error: action.error,
    };
  },
  [actionTypes.NONE](state: State = initialState, action) {
    return state;
  }
});
