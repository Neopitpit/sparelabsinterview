/**
 * index.js
 * @flow
 */

import { combineReducers } from 'redux';
import * as TranslinkReducer from './translinkReducer';

export default combineReducers(Object.assign(
    TranslinkReducer,
));
