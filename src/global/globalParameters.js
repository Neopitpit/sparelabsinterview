/**
 * globalParameters.js
 * @flow
 */

export const PROTOCOL = 'http://';
export const API = 'api.translink.ca/rttiapi/v1';
export const API_KEY = '8kQBWTJraF2vvNmBEe7e';
export const MAPBOX_KEY = 'pk.eyJ1IjoibmVvcGl0IiwiYSI6ImNqY2ZuanB1bDFzMXEyeG1tYmU0cHBxN28ifQ.3E10e5BOSKmjtfbdON5gaA';

export const translinkEndPoints = {
  buses: '/buses',
};

export const PULL_ALL_BUSES_NAME = 'PULL_ALL_BUSES';
export const PULL_ALL_BUSES_RATE = 2000;
